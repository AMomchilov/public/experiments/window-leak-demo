This simple app demonstrates a leak that I couldn't figure out. Closing the initial window (opened by the app on launch) does not deinit the `NSWindow` or its content view controller.

Interestingly, if you open up new windows from the menu (or command+n shortcut), and close those, they'll deinitialize properly. There's probably something about the way the first window is initialized by the template that I just don't understand.

This project was spun up from the macOS "App" template, using Storyboards for interfaces and the AppKit app delegate lifecycle. It was modified minimally to add logging for controller init/deinit, and to support the "New..." menu item. See the git diff for details.
