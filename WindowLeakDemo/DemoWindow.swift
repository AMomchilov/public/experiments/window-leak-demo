//
//  DemoWindow.swift
//  WindowLeakDemo
//
//  Created by Alexander Momchilov on 2021-04-28.
//

import AppKit

class DemoWindow: NSWindow {
    override init(
        contentRect: NSRect,
        styleMask style: NSWindow.StyleMask,
        backing backingStoreType: NSWindow.BackingStoreType,
        defer flag: Bool
    ) {
        super.init(
            contentRect: contentRect,
            styleMask: style,
            backing: backingStoreType,
            defer: flag
        )
        
        print(Self.self, #function)
    }
    
    deinit {
        print(Self.self, #function)
    }
}
