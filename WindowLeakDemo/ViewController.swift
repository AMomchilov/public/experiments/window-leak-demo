//
//  ViewController.swift
//  WindowLeakDemo
//
//  Created by Alexander Momchilov on 2021-04-28.
//

import Cocoa

class ViewController: NSViewController {

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        print(Self.self, #function)
    }
    
    deinit {
        print(Self.self, #function)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }


}

